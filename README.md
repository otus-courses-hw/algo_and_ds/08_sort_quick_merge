| sort             | elements  | comparisons | assignments | milliseconds |
|------------------|-----------|-------------|-------------|--------------|
| bubble           | 100       | 4884        | 6666        | 0            |
| bubble           | 1000      | 497789      | 703110      | 3            |
| bubble           | 10'000    | 49988672    | 76354344    | 420          |
| bubble           | 100'000   | 704850863   | 3193252076  | 46404        |
| bubble_shaker    | 100       | 4950        | 7185        | 0            |
| bubble_shaker    | 1000      | 499500      | 751662      | 3            |
| bubble_shaker    | 10'000    | 49995000    | 75291153    | 414          |
| bubble_shaker    | 100'000   | 704982704   | 3204325571  | 43160        |
| insertion        | 100       | 2429        | 6990        | 0            |
| insertion        | 1000      | 247476      | 739431      | 2            |
| insertion        | 10'000    | 24899474    | 74668425    | 264          |
| insertion        | 100'000   | 2499259284  | 3202510559  | 26072        |
| insertion_shift  | 100       | 2801        | 2900        | 0            |
| insertion_shift  | 1000      | 252981      | 253980      | 1            |
| insertion_shift  | 10'000    | 25229738    | 25239737    | 131          |
| insertion_binary | 100       |             | 2737        | 0            |
| insertion_binary | 1000      |             | 251699      | 1            |
| insertion_binary | 10'000    |             | 25046708    | 80           |
| insertion_binary | 100'000   |             | 2497286491  | 8194         |
| selection        | 100       | 4950        | 297         | 0            |
| selection        | 1000      | 499500      | 2997        | 13           |
| selection        | 10'000    | 49995000    | 29997       | 453          |
| selection        | 100'000   | 704982704   | 299997      | 49776        |
| heap             | 100       | 1647        | 4892        | 0            |
| heap             | 1000      | 26474       | 77444       | 0            |
| heap             | 10'000    | 364548      | 1055939     | 7            |
| heap             | 100'000   | 4644846     | 13389451    | 104          |
| heap             | 1'000'000 | 56337765    | 161940413   | 1382         |
| quick            | 100       | 681         | 1290        | 0            |
| quick            | 1000      | 10670       | 17370       | 1            |
| quick            | 10'000    | 155287      | 258678      | 7            |
| quick            | 100'000   | 2064471     | 3254157     | 32           |
| quick            | 1'000'000 | 25903318    | 39981642    | 390          |
| merge            | 100       | 530         | 530         | 0            |
| merge            | 1000      | 8708        | 8708        | 0            |
| merge            | 10'000    | 120475      | 120475      | 6            |
| merge            | 100'000   | 1536483     | 1536483     | 70           |
| merge            | 1'000'000 | 18672863    | 18672863    | 838          |
