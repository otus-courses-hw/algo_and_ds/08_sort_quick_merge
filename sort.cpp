#include "sort.hpp"
#include <vector>
#include <iterator>
#include <algorithm>

namespace sort
{
    namespace
    {
        info algo_stat;

        auto gt(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs > rhs;
        }
        auto gte(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs >= rhs;
        }
        auto lt(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs < rhs;
        }
        auto lte(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs <= rhs;
        }

        auto split(iter_t left, iter_t pivot)
        {
            auto m = std::prev(left);
            for (auto j = left; j <= pivot; ++j)
            {
                if (lte(*j, *pivot))
                {
                    std::swap(*(++m), *j);
                    algo_stat.assignment += 3;
                }
            }

            return m;
        }

        void helper_quick(iter_t left, iter_t right)
        {
            if (left >= right)
                return;

            auto pos = split(left, right);
            helper_quick(left, std::prev(pos));
            helper_quick(std::next(pos), right);
        }

        void merge_routine(iter_t left, iter_t middle, iter_t right)
        {
            std::vector<int> tmp(std::distance(left, right) + 1);

            auto i = left;
            auto j = std::next(middle);
            auto pos_where_to_insert = tmp.begin();
            while (i <= middle && j <= right)
            {
                if (lte(*i, *j))
                {
                    *pos_where_to_insert = *i;
                    algo_stat.assignment++;
                    ++i;
                }
                else
                {
                    *pos_where_to_insert = *j;
                    algo_stat.assignment++;
                    ++j;
                }

                ++pos_where_to_insert;
            }

            while (i <= middle)
            {
                *pos_where_to_insert = *i;
                ++pos_where_to_insert;
                ++i;
            }

            while (j <= right)
            {
                *pos_where_to_insert = *j;
                ++pos_where_to_insert;
                ++j;
            }

            std::swap_ranges(tmp.begin(), tmp.end(), left);
        }

        void helper_merge(iter_t left, iter_t right)
        {
            if (left >= right)
                return;

            auto dist = std::distance(left, right) / 2;
            auto middle = std::next(left, dist);
            helper_merge(left, middle);
            helper_merge(std::next(middle), right);
            merge_routine(left, middle, right);
        }
    }

    info quick(std::vector<int> &vec)
    {
        algo_stat.name = "quick";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        auto begin = vec.begin();
        auto pivot = std::prev(vec.end());
        helper_quick(begin, pivot);

        return algo_stat;
    }

    info merge(std::vector<int> &vec)
    {
        algo_stat.name = "merge";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        auto begin = vec.begin();
        auto end = std::prev(vec.end());
        helper_merge(begin, end);

        return algo_stat;
    }
}
