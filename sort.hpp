#include <vector>
#include <chrono>

namespace sort
{
    namespace {
        using const_iter = std::vector<int>::const_iterator;
        using iter = std::vector<int>::iterator;
    };
    using iter_t = iter;
    using ms = std::chrono::milliseconds;
    using diff_t = std::vector<int>::difference_type;

    struct info
    {
        const char *name;
        unsigned long elements;
        unsigned assignment;
        unsigned comparison;
        ms time;
    };

    info quick(std::vector<int>&);
    info merge(std::vector<int>&);
}
